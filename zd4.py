import sys,os
def suma(a):
	suma=0
	while True:
		f=a.read(1)
		if len(f)==0:
			break
		suma=suma+ord(f)
	return suma
def main():
	if len(sys.argv)>2:
		print('> 2 argumentów')
		sys.exit(0)
	try:
		a=open(sys.argv[1],'rb')
	except:
		print('Niema takiego pliku')
	result=suma(a)
	print(result)
main()
